
### Message Delimitation

All messages are terminated with `\r\n`. Server also terminates with `\r\n`.

### Auth Messages:

Must be first message sent by client to server

    Client → Server
    {
      type: “auth_hello”,
      user: “<user>”,
      pass: “<password>”
    }

Sent in response to ‘auth_hello’

    Server → Client
    {
      type: “auth_response”,
      lobby_id: int,
      success: boolean
    }
  
### Lobby Messages:

Can be sent anytime the client is in the lobby

    Client → Server
    {
      type: “lobby_getGameList”,
      @deprecated, to remove in the next version
      lobby_id: int // currently only 1, if separate lobbies are added in the future, use id passed as part of auth to get players for your lobby
    }

Sent in response to ‘lobby_getGameList’ AND send whenever the lobby status changes (players in/out, games create/destroy/entered)

    Server -> Client
    {
      type: “lobby_gameList”,
      games: [
        {
          game_id: int,
          game_name: “<game_name>”,
          map: “<map_name>”
        }
      ]
    }


@deprecated
Get the up to date player list inside a gameRoom

    Client -> Server
    {
      type: “lobby_getGamePlayerList”,
      game_id: int
    }


    Server -> Client
    {
      type: “lobby_gamePlayerList”,
      players: [
      {
          name: “<player1>”,
          race: “<human>”  
        }, …...
      ]
    }

Denotes client wants to specify game parameters -> create new game room
Then you automatically join the room

    Client -> Server
    {
      type: “lobby_hostGame”,
      game_name: “<game_name>”,
      map: “<map_name>”
    }

Sent in response to ‘lobby_hostGame’

    Server -> Client
    {
      type: “lobby_hostGameResponse”,
      success: boolean,
      game_id: int  // -1 on failure
    }

**@depercated**
Denotes client no longer wants to host a game (assume request succeeds)
Kicks everybody out from the game room and deletes it

    Client -> Server
    {
      type: “lobby_cancelHost”
    }

Denotes client wants to join a game

    Client -> Server
    {
      type: “lobby_joinGame”,
      game_id: int
    }

Sent in response to ‘lobby_joinGameResponse’

    Server -> Client
    {
      type: “lobby_joinGameResponse”,
      success: boolean
    }

Denotes client no longer wants to be part of game (assume request succeeds)
If the player that leaves is the host, the next on the list become the host

    Client -> Server
    {
      type: “lobby_leaveGameRoom”
    }

Sent arbitrarily by server, client should have async listener or occasionally check
Sent by server to denote game cancelled, player kicked

    Server -> Client
    {
      type: “lobby_gameKick”
    }

Sent arbitrarily by server, client should have async listener or occasionally check
Client should enter interactive game

    Server -> Client
    {
      type: “lobby_startGame”
    }

### Game Messages:

Sent by client to post a message, server sends authoritative message (in the order they must be executed)

    Client <-> Server
    {
      type: “game_action”,
      data: {
        Action: <gameaction>,
        Targets: [
            // Unit IDs...
        ]
        TargetColor: <target color>,
        TargetType: <target type>,
        GameCycle: <game cycle>,
        TargetLocation: {
            X : ...,
            Y : ...,
        }
      }
    }

Sent by client noting that the client is returning to lobby

    Client -> Server
    {
      type: “game_terminated”,
      playerWinner: <name>
    }

### Failure Messages:

Can be sent as a response to any malformed message sent by the client

    Server -> Client
    {
      type: “error”,
      message: “<error message>”
    }
