package warcraft.messages.request;

import retrofit.RetrofitError;
import warcraft.WarcraftServer;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Player;
import warcraft.web.ApiService;

import java.util.List;
import java.util.stream.Collectors;

public class game_terminated extends AbstractRequestMessage {

    public final String winner;

    public game_terminated(final String winner) {
        this.winner = winner;
    }

    @Override
    public void handleAuthenticated(final Player player, final GameRoomManager gameRoomManager) {

        final List<String> players = player.getGameRoom().getPlayers().stream().map(Player::getName).collect(Collectors.toList());

        try {
            if (WarcraftServer.WEB_MODE) {
                ApiService.factoryStat().winner(players, winner);
            }
        } catch (RetrofitError error) {
            // TODO error ignored
        }
    }

}
