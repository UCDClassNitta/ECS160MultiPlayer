package warcraft.messages.request;

import warcraft.entity.GameRoom;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Permitter;
import warcraft.entity.Player;
import warcraft.messages.response.lobby_hostGameResponse;

public class lobby_hostGame extends AbstractRequestMessage {

    public final String game_name;
    public final String map;

    public lobby_hostGame(final String game_name, final String map) {
        this.game_name = game_name;
        this.map = map;
    }

    @Override
    public void handleAuthenticated(Player player, GameRoomManager gameRoomManager) {
        GameRoom gameRoom = gameRoomManager.create(game_name, map);

        boolean success;
        try (Permitter.Permit movePlayerPermit = gameRoomManager.getMovePlayerToGameRoomPermit(player)) {
            if (movePlayerPermit.isValid()) {
                gameRoomManager.movePlayerToGameRoom(player, gameRoom);
                success = true;
            } else {
                gameRoomManager.removeGameRoom(gameRoom);
                success = false;
            }
        }

        new lobby_hostGameResponse(success, gameRoom).sendTo(player);
    }
}
