package warcraft.messages.request;

import warcraft.entity.GameRoomManager;
import warcraft.entity.Player;
import warcraft.messages.BaseMessage;

public abstract class AbstractRequestMessage extends BaseMessage {

    public void handle(Player player, GameRoomManager gameRoomManager) {
        if (player.isAuthticated()) {
            handleAuthenticated(player, gameRoomManager);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public void handleAuthenticated(Player player, GameRoomManager gameRoomManager) {
        throw new UnsupportedOperationException();
    }

}
