#include "Daemon.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#define PIPE_READ_INDEX     0
#define PIPE_WRITE_INDEX    1

#define DAEMON_REQUEST_PIPE_NAME            "RequestPipe"
#define DAEMON_REPLY_PIPE_NAME              "ReplyPipe"


#define DAEMON_ERR_MSG_DAEMON_EXISTS        "Daemon Already Exists"
#define DAEMON_ERR_MSG_RUN_DIR_FAIL         "Failed to Create Run Directory"
#define DAEMON_ERR_MSG_ERR_LOG_OPEN_FAIL    "Failed to Open Error Log"
#define DAEMON_ERR_MSG_LOG_DIR_FAIL         "Failed to Create Log Directory"
#define DAEMON_ERR_MSG_LOG_OPEN_FAIL        "Failed to Open Log"
#define DAEMON_ERR_MSG_REQUEST_PIPE_CREATE  "Failed to Create Request Pipe"
#define DAEMON_ERR_MSG_REPLY_PIPE_CREATE    "Failed to Create Reply Pipe"
#define DAEMON_ERR_MSG_REQUEST_PIPE_OPEN    "Failed to Open Request Pipe"
#define DAEMON_ERR_MSG_REPLY_PIPE_OPEN      "Failed to Open Reply Pipe"
#define DAEMON_ERR_MSG_REQUEST_PIPE_READ    "Failed to Read Request Pipe"
#define DAEMON_ERR_MSG_REPLY_PIPE_READ      "Failed to Read Reply Pipe"
#define DAEMON_ERR_MSG_REQUEST_PIPE_WRITE   "Failed to Write Request Pipe"
#define DAEMON_ERR_MSG_REPLY_PIPE_WRITE     "Failed to Write Reply Pipe"
#define DAEMON_ERR_MSG_SET_SID              "Failed to Create New SID"
#define DAEMON_ERR_MSG_SELECT               "Failed to Select"

CDaemon *CDaemon::DRunningDaemon = NULL;

CDaemon::CDaemon(const std::string &name){
    DRequestPipe = -1;
    DReplyPipe = -1;
    DErrorLogHandle = STDERR_FILENO;
    DLogHandle = STDOUT_FILENO;
    DSessionID = -1;

    DTerminated = false;
    DName = name;
    DLogPath = (std::string)"/var/log/" + name + "/";
    DRunPath = (std::string)"/var/run/" + name + "/";

}

CDaemon::~CDaemon(){
    if(-1 != DRequestPipe){
        close(DRequestPipe);
    }
    if(-1 != DReplyPipe){
        close(DReplyPipe);
    }
    if(-1 != DErrorLogHandle){
        close(DErrorLogHandle);
    }
    if(-1 != DLogHandle){
        close(DLogHandle);
    }
}

void CDaemon::StaticSignalHandler(int param){
    if(NULL != DRunningDaemon){
        DRunningDaemon->DaemonSignalHandler(param);
    }
}

void CDaemon::StaticCommandingSignalHandler(int param){
    if(NULL != DRunningDaemon){
        DRunningDaemon->CommandingSignalHandler(param);
    }    
}

bool CDaemon::PathExists(const std::string &path){
   struct stat StatBuffer;

   if(0 > stat(path.c_str(), &StatBuffer)){
         return false;
   }

   return S_ISDIR(StatBuffer.st_mode);
}

bool CDaemon::MakePath(const std::string &path){
    std::string SubPath;
    int PathLength = path.length();
    int SubPathLength;
    
    if(PathExists(path)){
        return true;   
    }
    
    SubPathLength = PathLength;
    if('/' == path[PathLength-1]){
        PathLength--;
        if(0 == PathLength){
            return true;
        }
    }
    SubPathLength = PathLength-1;
    while(SubPathLength && ('/' != path[SubPathLength])){
        SubPathLength--;
    }
    if(SubPathLength){
        SubPathLength--;
        if(SubPathLength){
            if(!MakePath(path.substr(0,SubPathLength+1))){
                return false;   
            }
        }
    }
    if(-1 == mkdir(path.substr(0,PathLength).c_str(), 0755)){
        if(EEXIST != errno){
            return false;    
        }
    }   
    return true;
}

bool CDaemon::ProcessMessages(long timeout){
    bool ReturnValue = false;
    unsigned char DataBuffer[65539];
    char *Arguements[1024];
    int ArguementCount,Index;
    SDaemonMessage *MessagePointer = (SDaemonMessage *)DataBuffer;

    do{
        MessagePointer->DHeader.DDataSize = sizeof(DataBuffer) - sizeof(SDaemonMessageHeader);
        if(ReceiveRequest(MessagePointer, timeout)){
            if(DAEMON_STOP_REQUEST ==  MessagePointer->DHeader.DRequest){
                DTerminated = true;
                return true;
            }
            else if(DAEMON_COMMAND_REQUEST == MessagePointer->DHeader.DRequest){
                ArguementCount = 0;
                Index = 0;
                while(Index < MessagePointer->DHeader.DDataSize){
                    Arguements[ArguementCount++] = (char *)MessagePointer->DData + Index;
                    Index += strlen((char *)MessagePointer->DData + Index) + 1;
                }
                ReturnValue = true;
                
                if(Command(ArguementCount, Arguements)){
                    MessagePointer->DData[0] = 1;
                }
                else{
                    MessagePointer->DData[0] = 0;
                }
                MessagePointer->DHeader.DRequest = DAEMON_RESULT_REPLY;
                MessagePointer->DHeader.DDataSize = 1;
                SendReply(MessagePointer);
            }
        }
        else{
            return ReturnValue;
        }
    }while(true);
    return false;
}

void CDaemon::DaemonSignalHandler(int param){
    bool AllowTermination;
    
    AllowTermination = this->SignalHandler(param);
    
    if(AllowTermination && (SIGTERM == param)){
        if(-1 != DErrorLogHandle){
            close(DErrorLogHandle);
            DErrorLogHandle = -1;
        }
        if(-1 != DLogHandle){
            close(DLogHandle);
            DLogHandle = -1;
        }
        if(-1 != DRequestPipe){
            close(DRequestPipe);
            DRequestPipe = -1;
        }
        if(-1 != DReplyPipe){
            close(DReplyPipe);
            DReplyPipe = -1;
        }
        unlink((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str());
        unlink((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str());
        unlink((DRunPath + DName + ".pid").c_str());      
        exit(1);  
    }
}

void CDaemon::CommandingSignalHandler(int param){
    
    if(SIGINT == param){
        FILE *FileHandle;
        char CharBuffer[64];
        int ProcessID;
        
        FileHandle = fopen((DRunPath + DName + ".pid").c_str(), "r");
        if(NULL == FileHandle){
            return;   
        }
        fscanf(FileHandle, "%d", &ProcessID);
        fclose(FileHandle);
        kill(ProcessID, SIGINT);
    }
    else if(SIGTERM == param){
        exit(1);   
    }
}

bool CDaemon::ReceiveRequest(SDaemonMessage *message, long timeout){
    long MaxDataSize = message->DHeader.DDataSize;
    long ReadSize;
    unsigned char Buffer;
    fd_set ReadFileDescriptors;
    struct timeval SelectTimeout;
    int ReturnStatus;
    
    if(-1 == DRequestPipe){
        return false;
    }
    while(true){
        for(int Index = 0; Index < DPollingFDs.size(); Index++){
            DPollingFDs[Index].revents = 0;
        }
        ReturnStatus = poll(DPollingFDs.data(), DPollingFDs.size(), timeout);
        if(0 > ReturnStatus){
            LogError(DAEMON_ERR_MSG_SELECT);
            return false;
        }
        if(0 == ReturnStatus){
            DLastError = "Request Timeout";
            return false;
        }
        for(int Index = 1; Index < DPollingFDs.size(); Index++){
            if(DPollingFDs[Index].revents){
                FileEvent(DPollingFDs[Index].fd,DPollingFDs[Index].revents);    
            }
        }
        if(DPollingFDs[0].revents){
            if(-1 == read(DRequestPipe, message, sizeof(SDaemonMessageHeader))){
                LogError(DAEMON_ERR_MSG_REQUEST_PIPE_READ);
                return false;
            }
        
            if(MaxDataSize >= message->DHeader.DDataSize){
                ReadSize = message->DHeader.DDataSize;
            }
            else{
                ReadSize = MaxDataSize;
            }
            if(ReadSize){
                if(-1 == read(DRequestPipe, message->DData, ReadSize)){
                    LogError(DAEMON_ERR_MSG_REQUEST_PIPE_READ);
                    return false;
                }
                while(ReadSize < message->DHeader.DDataSize){
                    if(-1 == read(DRequestPipe, &Buffer, 1)){
                        LogError(DAEMON_ERR_MSG_REQUEST_PIPE_READ);
                        return false;
                    }
                    ReadSize++;
                }
            }
            return true;
        }
    }
}

bool CDaemon::SendReply(SDaemonMessage *message){
    if(-1 == DReplyPipe){
        return false;
    }
    if(-1 == write(DReplyPipe, message, message->DHeader.DDataSize + sizeof(SDaemonMessageHeader))){
        LogError(DAEMON_ERR_MSG_REPLY_PIPE_WRITE);
        return false;
    }
    return true;
}

bool CDaemon::RequestNotification(int fd, short events){
    struct pollfd TempPollFD;
    if(0 == DPollingFDs.size()){
        return false;
    }
    if(fd == DPollingFDs[0].fd){
        return false;
    }
    for(int Index = 1; Index < DPollingFDs.size(); Index++){
        if(fd == DPollingFDs[Index].fd){
            if(0 == events){
                DPollingFDs.erase(DPollingFDs.begin() + Index);
                return true;
            }
            else{
                DPollingFDs[Index].events = events;   
            }
        }
    }
    if(0 == events){
        return false;   
    }
    TempPollFD.fd = fd;
    TempPollFD.events = events;
    DPollingFDs.push_back(TempPollFD);
    return true;
}

void CDaemon::OutputToLogFile(int handle, const std::string &message, bool showtime){
    if(-1 != handle){
        std::string OutputMessage;
        std::string TimeString;
        time_t CurrentTime = time(NULL);
        
        TimeString = ctime(&CurrentTime);
        if(!showtime){
            OutputMessage = message;
        }
        else{
            OutputMessage = (std::string)"[" + TimeString.substr(0, TimeString.length() - 1);
            OutputMessage += "] ";
            OutputMessage += message;
            OutputMessage += "\n";
        }
        write(handle, OutputMessage.c_str(), OutputMessage.length());
    }    
}

void CDaemon::LogError(const std::string &message, bool showtime){
    OutputToLogFile(DErrorLogHandle, message, showtime);
    DLastError = message;
}

void CDaemon::Log(const std::string &message, bool showtime){
    OutputToLogFile(DLogHandle, message, showtime);
}

void CDaemon::PrintError(const char *format, ...){
    unsigned char DataBuffer[65539];
    va_list ArguementList;
    SDaemonMessage *MessagePointer = (SDaemonMessage *)DataBuffer;
    
    va_start(ArguementList, format);
    MessagePointer->DHeader.DRequest = DAEMON_ERROR_REPLY;
    MessagePointer->DHeader.DDataSize = vsprintf((char *)MessagePointer->DData, format, ArguementList) + 1;
    va_end (ArguementList);
    SendReply(MessagePointer);
}

void CDaemon::Print(const char *format, ...){
    unsigned char DataBuffer[65539];
    va_list ArguementList;
    SDaemonMessage *MessagePointer = (SDaemonMessage *)DataBuffer;
    
    va_start(ArguementList, format);
    MessagePointer->DHeader.DRequest = DAEMON_OUTPUT_REPLY;
    MessagePointer->DHeader.DDataSize = vsprintf((char *)MessagePointer->DData, format, ArguementList) + 1;
    va_end (ArguementList);
    SendReply(MessagePointer);
}

bool CDaemon::Start(){
    std::string TempString;
    int FileHandle;
    int StartPipe[2];
    pid_t ProcessID;
    char ByteBuffer[2];
    char CharBuffer[64];
    bool StartError;
    bool ReturnStatus;

    if(!PreForkStart()){
        return false;
    }
    if(-1 == pipe(StartPipe)){
        PreForkCleanup();
        DLastError = "Pipe Failed";
        return false;
    }
    ProcessID = fork();
    if(0 > ProcessID){
        DLastError = "Fork Failed";
        return false;
    }
    if(0 < ProcessID){
        close(StartPipe[PIPE_WRITE_INDEX]);
        DLastError = "";
        ReturnStatus = true;
        read(StartPipe[PIPE_READ_INDEX], ByteBuffer, 1);
        if(0 == ByteBuffer[0]){
            ByteBuffer[1] = '\0';
            do{
                read(StartPipe[PIPE_READ_INDEX], ByteBuffer, 1);
                DLastError += ByteBuffer;
            }while('\0' != ByteBuffer[0]);
            ReturnStatus = false;
        }
        close(StartPipe[PIPE_READ_INDEX]);
        return ReturnStatus;
    }
    close(StartPipe[PIPE_READ_INDEX]);

    ProcessID = getpid();
    umask(0);
    //Make the DRunPath
    if(!MakePath(DRunPath.substr(0,DRunPath.length() - 1))){
        DLastError = DAEMON_ERR_MSG_RUN_DIR_FAIL;
        return false;
    }

    FileHandle = open((DRunPath + DName + ".pid").c_str(), O_RDWR | O_EXCL | O_CREAT, 0660);
    if(-1 == FileHandle){
        PostForkCleanup();
        DLastError = DAEMON_ERR_MSG_DAEMON_EXISTS;
        ByteBuffer[0] = 0;
        write(StartPipe[PIPE_WRITE_INDEX], ByteBuffer, 1);
        write(StartPipe[PIPE_WRITE_INDEX], DLastError.c_str(), DLastError.length() + 1);
        close(StartPipe[PIPE_WRITE_INDEX]);
        exit(EXIT_FAILURE);
    }
    sprintf(CharBuffer, "%d", ProcessID);
    TempString = CharBuffer;
    write(FileHandle, TempString.c_str(), TempString.length());
    close(FileHandle);
    StartError = true;
    ReturnStatus = false;

    //Make the DLogPath
    if(!MakePath(DLogPath.substr(0,DLogPath.length() - 1))){
        DLastError = DAEMON_ERR_MSG_LOG_DIR_FAIL;
        return false;
    }

    DErrorLogHandle = open((DLogPath + "Error.log").c_str(), O_WRONLY | O_CREAT | O_APPEND, 0660);
    if(-1 == DErrorLogHandle){
        DLastError = DAEMON_ERR_MSG_ERR_LOG_OPEN_FAIL;
        goto CDaemonStartExit;
    }

    DLogHandle = open((DLogPath + DName + ".log").c_str(), O_WRONLY | O_CREAT | O_APPEND, 0660);
    if(-1 == DLogHandle){
        DLastError = DAEMON_ERR_MSG_LOG_OPEN_FAIL;
        goto CDaemonStartExit;
    }

    if(-1 == mkfifo((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str(), 0660)){
        DLastError = DAEMON_ERR_MSG_REQUEST_PIPE_CREATE;
        goto CDaemonStartExit;
    }

    if(-1 == mkfifo((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str(), 0660)){
        DLastError = DAEMON_ERR_MSG_REPLY_PIPE_CREATE;
        goto CDaemonStartExit;
    }
    DRequestPipe = open((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str(), O_RDWR);
    if(-1 == DRequestPipe){
        DLastError = DAEMON_ERR_MSG_REQUEST_PIPE_OPEN;
        goto CDaemonStartExit;
    }
    DReplyPipe = open((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str(), O_RDWR);
    if(-1 == DRequestPipe){
        DLastError = DAEMON_ERR_MSG_REPLY_PIPE_OPEN;
        goto CDaemonStartExit;
    }
    DSessionID  = setsid();
    if(0 > DSessionID){
        DLastError = DAEMON_ERR_MSG_SET_SID;
        goto CDaemonStartExit;
    }

    
    //Signal Parent that successfully started daemon
    ByteBuffer[0] = 1;
    write(StartPipe[PIPE_WRITE_INDEX], ByteBuffer, 1);

    StartError = false;
    close(StartPipe[PIPE_WRITE_INDEX]);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    DRunningDaemon = this;
    DPreviousSignalTerminate = signal(SIGTERM, StaticSignalHandler);
    DPreviousSignalInterrupt = signal(SIGINT, StaticSignalHandler);
    DPollingFDs.resize(1);
    DPollingFDs[0].fd = DRequestPipe;
    DPollingFDs[0].events = POLLIN;
    DPollingFDs[0].revents = 0;
    ReturnStatus = Execute();
CDaemonStartExit:
    if(-1 != DErrorLogHandle){
        if(StartError){
            LogError(DLastError);
        }
        close(DErrorLogHandle);
        DErrorLogHandle = -1;
    }
    if(-1 != DLogHandle){
        close(DLogHandle);
        DLogHandle = -1;
    }
    if(-1 != DRequestPipe){
        close(DRequestPipe);
        DRequestPipe = -1;
    }
    if(-1 != DReplyPipe){
        close(DReplyPipe);
        DReplyPipe = -1;
    }
    unlink((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str());
    unlink((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str());
    unlink((DRunPath + DName + ".pid").c_str());
    if(StartError){
        PostForkCleanup();
        ByteBuffer[0] = 0;
        write(StartPipe[PIPE_WRITE_INDEX], ByteBuffer, 1);
        write(StartPipe[PIPE_WRITE_INDEX], DLastError.c_str(), DLastError.length() + 1);
        close(StartPipe[PIPE_WRITE_INDEX]);
    }
    exit(ReturnStatus ? EXIT_SUCCESS : EXIT_FAILURE);
    return ReturnStatus;
}

bool CDaemon::Stop(){
    SDaemonMessage Message;
    
    if(!IsRunning()){
        DLastError = "Daemon is not running";
        return false;
    }

    Message.DHeader.DRequest = DAEMON_STOP_REQUEST;
    Message.DHeader.DDataSize = 0;

    return SendMessage(&Message);
}

bool CDaemon::IsRunning(){
    FILE *FileHandle;
    char CharBuffer[64];
    int ProcessID;
    
    FileHandle = fopen((DRunPath + DName + ".pid").c_str(), "r");
    if(NULL == FileHandle){
        return false;   
    }
    fscanf(FileHandle, "%d", &ProcessID);
    fclose(FileHandle);
    sprintf(CharBuffer,"%d", ProcessID);
    
    return PathExists((std::string)"/proc/" + CharBuffer);
}

bool CDaemon::Cleanup(){
    if(!IsRunning()){
        unlink((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str());
        unlink((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str());
        unlink((DRunPath + DName + ".pid").c_str());
        return true;
    }
    DLastError = "Daemon is still running";
    return false;
}

bool CDaemon::CommandRequest(int argc, char *argv[]){
    SDaemonMessageRef MessageRef;
    unsigned char DataBuffer[65539];
    char *Pointer;
    int CommandLength = 0;
    bool ReturnValue = false;
    
    if(!IsRunning()){
        DLastError = "Daemon is not running";
        return false;   
    }
    for(int Index = 0; Index < argc; Index++){
        CommandLength += strlen(argv[Index]) + 1;
    }
    if(!CommandLength || (0x10000 <= CommandLength)){
        return false;   
    }
    MessageRef = (SDaemonMessageRef)DataBuffer;
    Pointer = (char *)&MessageRef->DData[0];
    for(int Index = 0; Index < argc; Index++){
        strcpy(Pointer, argv[Index]);
        Pointer += strlen(argv[Index]) + 1;        
    }    
    MessageRef->DHeader.DRequest = DAEMON_COMMAND_REQUEST;
    MessageRef->DHeader.DDataSize = CommandLength;
    
    DRunningDaemon = this;
    DPreviousSignalTerminate = signal(SIGTERM, StaticCommandingSignalHandler);
    DPreviousSignalInterrupt = signal(SIGINT, StaticCommandingSignalHandler);
    if(SendMessage(MessageRef)){
        // wait for reply
        do{
            MessageRef->DHeader.DDataSize = sizeof(DataBuffer) - sizeof(SDaemonMessageHeader);
            if(ReceiveMessage(MessageRef, 1000)){
                if(DAEMON_RESULT_REPLY == MessageRef->DHeader.DRequest){
                    ReturnValue = MessageRef->DData;
                    break;
                }
                else if(DAEMON_ERROR_REPLY == MessageRef->DHeader.DRequest){
                    fprintf(stderr, "%s",(const char *)MessageRef->DData);
                    fflush(stderr);
                }
                else if(DAEMON_OUTPUT_REPLY == MessageRef->DHeader.DRequest){
                    fprintf(stdout, "%s",(const char *)MessageRef->DData);
                    fflush(stdout);
                }
            }
        }while(true);
    }
    signal(SIGTERM, DPreviousSignalTerminate);
    signal(SIGINT, DPreviousSignalInterrupt);    
    DRunningDaemon = NULL;
    return ReturnValue;
}

bool CDaemon::SendMessage(SDaemonMessage *message){
    if(-1 == DRequestPipe){
        DRequestPipe = open((DRunPath + DAEMON_REQUEST_PIPE_NAME).c_str(), O_RDWR);
        if(-1 == DRequestPipe){
            DLastError = DAEMON_ERR_MSG_REQUEST_PIPE_OPEN;
            return false;
        }
    }

    if(-1 == write(DRequestPipe, message, message->DHeader.DDataSize + sizeof(SDaemonMessageHeader))){
        DLastError = DAEMON_ERR_MSG_REQUEST_PIPE_WRITE;
        return false;
    }
    return true;
}

bool CDaemon::ReceiveMessage(SDaemonMessage *message, long timeout){
    long MaxDataSize = message->DHeader.DDataSize;
    long ReadSize;
    unsigned char Buffer;
    struct pollfd RequestPollFD;
    int ReturnStatus;

    if(-1 == DReplyPipe){
        DReplyPipe = open((DRunPath + DAEMON_REPLY_PIPE_NAME).c_str(), O_RDWR);
        if(-1 == DReplyPipe){
            DLastError = DAEMON_ERR_MSG_REPLY_PIPE_OPEN;
            return false;
        }
    }

    RequestPollFD.fd = DReplyPipe;
    RequestPollFD.events = POLLIN;
    RequestPollFD.revents = 0;
    ReturnStatus = poll(&RequestPollFD, 1, timeout);
    if(0 > ReturnStatus){
        DLastError = "Select Error";
        return false;
    }
    if(0 == ReturnStatus){
        DLastError = "Reply Timeout";
        return false;
    }
    if(-1 == read(DReplyPipe, message, sizeof(SDaemonMessageHeader))){
        DLastError = DAEMON_ERR_MSG_REPLY_PIPE_READ;
        return false;
    }
    if(MaxDataSize >= message->DHeader.DDataSize){
        ReadSize = message->DHeader.DDataSize;
    }
    else{
        ReadSize = MaxDataSize;
    }
    if(ReadSize){
        if(-1 == read(DReplyPipe, message->DData, ReadSize)){
            DLastError = DAEMON_ERR_MSG_REPLY_PIPE_READ;
            return false;
        }
        while(ReadSize < message->DHeader.DDataSize){
            if(-1 == read(DReplyPipe, &Buffer, 1)){
                DLastError = DAEMON_ERR_MSG_REPLY_PIPE_READ;
                return false;
            }
            ReadSize++;
        }
    }
    return true;
}

