#include "SleepDaemon.h"

CSleepDaemon::CSleepDaemon(long sleeptime) : CDaemon("sleepd"){
    DSleepTime = sleeptime;
}

CSleepDaemon::~CSleepDaemon(){

}

bool CSleepDaemon::SignalHandler(int param){
    DWaiting = false;
    return true;
}

bool CSleepDaemon::PreForkStart(){
    return true;
}

bool CSleepDaemon::PreForkCleanup(){
    return true;
}

bool CSleepDaemon::PostForkCleanup(){
    return true;
}

bool CSleepDaemon::Execute(){
    
    while(!Terminated()){
        ProcessMessages(DSleepTime);
        if(!Terminated()){
            Log("Heart Beat");
        }   
    }
    
    return true;
}

bool CSleepDaemon::Command(int argc, char *argv[]){
    std::string OutputString;

    if((std::string)argv[0] == "wait"){
        Print("Waiting ");
        DWaiting = true;
        do{
            sleep(1);
            Print(".");
        }while(DWaiting);
    }
    else{
        Print("Command:");
        for(int Index = 0; Index < argc; Index++){
            OutputString += argv[Index];
            OutputString += " ";
            Print(" %s", argv[Index]);
        }
        Log(OutputString);
        sleep(1);
        Print("\n");
    }
    return true;
}

bool CSleepDaemon::FileEvent(int fd, short event){
    return true;
}
