package warcraft.messages.response;

import warcraft.entity.GameRoom;
import warcraft.view.PlayerItem;

import java.util.Collection;
import java.util.stream.Collectors;

public class lobby_gamePlayerList extends AbstractResponseMessage {

    public final int game_id;
    public final Collection<PlayerItem> players;

    public lobby_gamePlayerList(final GameRoom gameRoom) {
        game_id = gameRoom.getGameRoomId();
        players = gameRoom.getPlayers().stream().map(PlayerItem::new).collect(Collectors.toList());
    }

}
