#include "SleepDaemon.h"
#include <stdio.h>

int main(int argc, char *argv[]){
    CSleepDaemon Daemon(5000);
    std::string Arguement;

    if(1 == argc){
        printf("Usage: sleepd (start | stop | stat | cleanup)\n");
        return 0;    
    }
    if(2 == argc){
        Arguement = argv[1];
        if(Arguement == "start"){
            printf("Starting ");
            if(!Daemon.Start()){
                printf("Error: %s",(Daemon.LastError() + "\n").c_str());
                return 0;
            }
            printf("Success\n");
            return 0;
        }
        else if(Arguement == "stop"){
            printf("Stopping ");
            fflush(stdout);
            if(!Daemon.Stop()){
                printf("Error: %s",(Daemon.LastError() + "\n").c_str());
                return 0;
            }
            printf("Success\n");
            return 0;
        }
        else if(Arguement == "stat"){
            if(Daemon.IsRunning()){
                printf("Daemon is running\n");
            }
            else{
                printf("Daemon is not running\n");
            }
            return 0;
        }
        else if(Arguement == "cleanup"){
            if(!Daemon.Cleanup()){
                printf("Error: %s",(Daemon.LastError() + "\n").c_str());
                return 0;
            }
            return 0;
        }
    }
    if(!Daemon.CommandRequest(argc-1, argv + 1)){
        printf("Error: %s",(Daemon.LastError() + "\n").c_str());
    }
    return 0;
}


