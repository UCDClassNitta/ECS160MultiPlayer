#ifndef DAEMON_H
#define DAEMON_H

#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <poll.h>
#include <vector>

#define DAEMON_STOP_REQUEST     0x0001
#define DAEMON_COMMAND_REQUEST  0x0002

#define DAEMON_RESULT_REPLY     0x0001
#define DAEMON_OUTPUT_REPLY     0x0002
#define DAEMON_ERROR_REPLY      0x0003

typedef struct{
    unsigned short DRequest;
    unsigned short DDataSize;
} SDaemonMessageHeader, *SDaemonMessageHeaderRef;

typedef struct{
    SDaemonMessageHeader DHeader;
    unsigned char DData[1];
} SDaemonMessage, *SDaemonMessageRef;

class CDaemon{
    protected:
        static CDaemon *DRunningDaemon;
        void (*DPreviousSignalTerminate)(int);
        void (*DPreviousSignalInterrupt)(int);
        int DRequestPipe;
        int DReplyPipe;
        int DErrorLogHandle;
        int DLogHandle;
        pid_t DSessionID;
        volatile bool DTerminated;

        std::string DName;
        std::string DRunPath;
        std::string DLogPath;
        std::string DLastError;
        
        std::vector< struct pollfd > DPollingFDs;

        bool Terminated(){return DTerminated;};

        virtual bool SignalHandler(int param) = 0;
        virtual bool PreForkStart() = 0;
        virtual bool PreForkCleanup() = 0;
        virtual bool PostForkCleanup() = 0;
        virtual bool Execute() = 0;
        virtual bool Command(int argc, char *argv[]) = 0;
        virtual bool FileEvent(int fd, short event) = 0;
        
        static void StaticSignalHandler(int param);
        static void StaticCommandingSignalHandler(int param);
        static bool PathExists(const std::string &path);
        static bool MakePath(const std::string &path);

        bool ProcessMessages(long timeout);
        void DaemonSignalHandler(int param);
        void CommandingSignalHandler(int param);
        bool ReceiveRequest(SDaemonMessage *message, long timeout);
        bool SendReply(SDaemonMessage *message);
        bool RequestNotification(int fd, short events);

        void OutputToLogFile(int handle, const std::string &message, bool showtime);
        void LogError(const std::string &message, bool showtime = true);
        void Log(const std::string &message, bool showtime = true);
        void PrintError(const char *format, ...);
        void Print(const char *format, ...);

    public:
        CDaemon(const std::string &name);
        virtual ~CDaemon();

        bool Start();
        bool Stop();
        bool IsRunning();
        bool Cleanup();

        bool CommandRequest(int argc, char *argv[]);
        bool SendMessage(SDaemonMessage *message);
        bool ReceiveMessage(SDaemonMessage *message, long timeout);

        std::string LastError() const {return DLastError;};
};


#endif
