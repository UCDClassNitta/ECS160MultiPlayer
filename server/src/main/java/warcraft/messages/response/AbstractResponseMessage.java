package warcraft.messages.response;

import warcraft.entity.GameRoom;
import warcraft.entity.Player;
import warcraft.messages.BaseMessage;

public class AbstractResponseMessage extends BaseMessage {

    public void sendTo(Player player) {
        player.getChannel().writeAndFlush(this);
    }

    public final void sendTo(GameRoom gameRoom) {
        gameRoom.getChannels().flushAndWrite(this);
    }
}
