package warcraft.messages.response;

public class auth_response extends AbstractResponseMessage {

    public final int lobby_id;
    public final boolean success;

    public auth_response(final int lobby_id, final boolean success) {
        this.lobby_id = lobby_id;
        this.success = success;
    }

}
