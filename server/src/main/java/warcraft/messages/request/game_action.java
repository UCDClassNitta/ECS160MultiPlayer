package warcraft.messages.request;

import com.google.gson.JsonObject;
import warcraft.entity.GameRoom;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Player;

import java.util.Collection;

public class game_action extends AbstractRequestMessage {

    public final JsonObject data;

    public game_action(final JsonObject data) {
        this.data = data;
    }

    @Override
    public void handleAuthenticated(Player player, GameRoomManager gameRoomManager) {

        GameRoom room = player.getGameRoom();

        room.addMessage(player, this);

        Collection<JsonObject> broadcast = room.getBroadcastMessage();

        if (broadcast != null) {
            for (JsonObject object : broadcast) {
                new warcraft.messages.response.game_action(object).sendTo(player.getGameRoom());
            }
        }

//        new warcraft.messages.response.game_action(data).sendTo(player.getGameRoom());
    }
}
