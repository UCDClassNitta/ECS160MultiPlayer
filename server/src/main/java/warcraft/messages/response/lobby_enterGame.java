package warcraft.messages.response;

import warcraft.entity.GameRoom;
import warcraft.entity.Player;
import warcraft.view.PlayerItem;

public class lobby_enterGame extends AbstractResponseMessage {
    public final int game_id;
    public final PlayerItem player;

    public lobby_enterGame(final Player player, final GameRoom gameRoom) {
        this.player = new PlayerItem(player);
        this.game_id = gameRoom.getGameRoomId();
    }
}
