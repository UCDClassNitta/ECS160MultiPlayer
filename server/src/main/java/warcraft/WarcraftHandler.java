package warcraft;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Player;
import warcraft.messages.request.AbstractRequestMessage;
import warcraft.messages.response.error.error;
import warcraft.web.ApiService;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

@ChannelHandler.Sharable
public class WarcraftHandler extends SimpleChannelInboundHandler<AbstractRequestMessage> {

    private final static ConcurrentHashMap<String, Player> channelIdToPlayer = new ConcurrentHashMap<>();

    private final GameRoomManager gameRoomManager;

    public WarcraftHandler(GameRoomManager gameRoomManager) {
        this.gameRoomManager = gameRoomManager;
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        Player player = new Player(ctx.channel());
        channelIdToPlayer.put(ctx.channel().toString(), player);
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, AbstractRequestMessage message) throws Exception {

        Player player = channelIdToPlayer.get(ctx.channel().toString());
        if (player == null) {
            throw new Exception("The user should exist");
        }

        message.handle(player, this.gameRoomManager);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        WarcraftServer.LOGGER.log(Level.SEVERE, "Error in WarcraftHandler: " + cause.getMessage(), cause);
        ctx.writeAndFlush(new error("Failure in WarcraftHandler"));
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        Player player = channelIdToPlayer.get(ctx.channel().toString());


        // Temporary put here the notification of the web server that the player unlogged
        if (player.isAuthticated()) {
            // TODO if the web server existed, it would work
            if (WarcraftServer.WEB_MODE) {
                ApiService.factoryAuth().logout(player.getName());
            }
        }
    }
}
