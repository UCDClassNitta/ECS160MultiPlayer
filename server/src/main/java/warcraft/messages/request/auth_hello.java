package warcraft.messages.request;

import retrofit.RetrofitError;
import warcraft.WarcraftServer;
import warcraft.entity.GameRoom;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Permitter;
import warcraft.entity.Player;
import warcraft.messages.response.auth_response;
import warcraft.messages.response.error.AuthenticationError;
import warcraft.messages.response.lobby_gamePlayerList;
import warcraft.web.ApiService;

public class auth_hello extends AbstractRequestMessage {

    public final String user;
    public final String pass;

    public auth_hello(final String user, final String pass) {
        this.user = user;
        this.pass = pass;
    }

    @Override
    public void handle(Player player, GameRoomManager gameRoomManager) {
        try {
            boolean success;

            if (WarcraftServer.WEB_MODE) {
                success = ApiService.factoryAuth().login(user, pass);
            } else {
                success = true;
            }

            if (success) {
                try (Permitter.Permit login = player.getLoginPermit()) {
                    if (login.isValid()) {
                        player.login(user);
                        success = true;
                    } else {
                        success = false;
                    }
                }
            }

            new auth_response(1, success).sendTo(player);

            if (!success) {
                return;
            }

            //TODO now by default logs you inside room 0
            GameRoom gameRoom = gameRoomManager.getGameRooms().get(0);
            try (
                    Permitter.Permit gameroomPermit = gameRoom.getPlayerSubscribePermit();
                    Permitter.Permit playerPermit = player.getSubscribePermit();
            ) {
                player.subscribe(gameRoom);
                gameRoom.playerSubscribe(player);
            }
            new lobby_gamePlayerList(gameRoom).sendTo(gameRoom);

        } catch (RetrofitError err) {
            new auth_response(1, false).sendTo(player);
            new AuthenticationError.notAuthenticated().sendTo(player);
        }

    }


}
