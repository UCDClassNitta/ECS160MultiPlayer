package warcraft.messages.response.error;

import warcraft.messages.response.AbstractResponseMessage;

public class error extends AbstractResponseMessage {

    public final String subtype = this.getClass().getSimpleName();

    public String message;

    public error() {
        this("");
    }

    public error(final String message) {
        this.message = message;
        this.type = "error";
    }

}
