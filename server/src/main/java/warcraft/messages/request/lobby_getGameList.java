package warcraft.messages.request;

import warcraft.entity.GameRoomManager;
import warcraft.entity.Player;
import warcraft.messages.response.lobby_gameList;

public class lobby_getGameList extends AbstractRequestMessage {

    public int lobby_id;

    public lobby_getGameList(final int lobby_id) {
        this.lobby_id = lobby_id;
    }

    @Override
    public void handleAuthenticated(Player player, GameRoomManager gameRoomManager) {
        new lobby_gameList(gameRoomManager).sendTo(player);
    }
}