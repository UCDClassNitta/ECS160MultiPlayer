package warcraft.entity;

import com.google.gson.JsonObject;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import warcraft.WarcraftServer;
import warcraft.messages.request.game_action;

import java.util.*;

public class GameRoom extends Permitter {

    public enum GameRoomState {
        WATING, PLAYNG, ENDED
    }

    private final int roomId;
    private final String name;
    private final String map;
    private final HashSet<Player> players = new HashSet<>();
    private GameRoomState state;

    private final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private HashMap<Integer, Queue<JsonObject>> actions = new HashMap<>();

    public GameRoom(final int roomId, final String name, final String map) {
        state = GameRoomState.WATING;
        this.roomId = roomId;
        this.name = name;
        this.map = map;
    }

    public Permit getPlayerSubscribePermit() {
        return issuePermit("playerSubscribe", state == GameRoomState.WATING);
    }

    public void playerSubscribe(final Player subscriber) {
        checkForPermit("playerSubscribe");

        players.add(subscriber);
        channels.add(subscriber.getChannel());

        // TODO hack for messaging
//        actions.put(subscriber, new ArrayDeque<>());
    }

    public Permit getPlayerUnsubscribePermit(final Player subscriber) {
        return issuePermit("playerUnsubscribe", players.contains(subscriber));
    }

    void playerUnsubscribe(final Player subscriber) {
        players.remove(subscriber);
        channels.remove(subscriber.getChannel());
    }

    public void addMessage(final Player player, final game_action action) {
        try (Permit permit = selfPermit()) {

            final int playerColor = action.data.get("PlayerColor").getAsInt();

            final Queue<JsonObject> queue = actions.computeIfAbsent(playerColor, ArrayDeque::new);

            queue.add(action.data);
        }
    }

    public ArrayList<JsonObject> getBroadcastMessage() {
        try (Permit permit = selfPermit()) {

            final Collection<Queue<JsonObject>> queues = actions.values();

            for (final Queue<JsonObject> queue : queues) {
                if (queue.size() < WarcraftServer.TURN_LENGTH) {
                    return null;
                }
            }

            final ArrayList<JsonObject> tosend = new ArrayList<>();

            for (final Queue<JsonObject> queue : queues) {
                for (int i = 0; i < WarcraftServer.TURN_LENGTH; i++) {
                    tosend.add(queue.poll());
                }
            }

            return tosend;
        }
    }

    private static class PlayerData {
        final Queue<JsonObject> queue = new LinkedList<>();
        int lastCycle = 0;
    }

    // ####################### GETTER AND SETTER SECTION #########################à

    public ChannelGroup getChannels() {
        return this.channels;
    }

    public Set<Player> getPlayers() {
        try (Permit permit = selfPermit()) {
            return new HashSet<>(players);
        }
    }

    public int getGameRoomId() {
        return roomId;
    }

    public String getName() {
        return name;
    }

    public String getMap() {
        return map;
    }

    public GameRoomState getState() {
        try (Permit permit = selfPermit()) {
            return state;
        }
    }
}
