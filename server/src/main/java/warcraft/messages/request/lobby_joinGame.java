package warcraft.messages.request;

import warcraft.entity.GameRoom;
import warcraft.entity.GameRoomManager;
import warcraft.entity.Permitter;
import warcraft.entity.Player;
import warcraft.messages.response.lobby_enterGame;
import warcraft.messages.response.lobby_joinGameResponse;

public class lobby_joinGame extends AbstractRequestMessage {

    public final int game_id;

    public lobby_joinGame(final int game_id) {
        this.game_id = game_id;
    }

    @Override
    public void handleAuthenticated(Player player, GameRoomManager gameRoomManager) {

        GameRoom gameRoom = gameRoomManager.getGameRoom(game_id);
        boolean success;
        try (
                Permitter.Permit movePlayerPermit = gameRoomManager.getMovePlayerToGameRoomPermit(player);
                Permitter.Permit gameRoomSubPermit = gameRoom.getPlayerSubscribePermit();
        ) {
            if (movePlayerPermit.isValid() && gameRoomSubPermit.isValid()) {

                gameRoomManager.movePlayerToGameRoom(player, gameRoom);
                gameRoom.playerSubscribe(player);
                success = true;

            } else {
                success = false;
            }
        }

        new lobby_joinGameResponse(success).sendTo(player);

        if (success) {
            new lobby_enterGame(player, gameRoom).sendTo(gameRoom);
        }
    }
}
