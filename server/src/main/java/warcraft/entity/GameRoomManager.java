package warcraft.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class GameRoomManager extends Permitter {

    private final HashMap<Integer, GameRoom> gameRooms = new HashMap<>();
    private final HashSet<Player> players = new HashSet<>();

    {
        try (Permitter.Permit permit = selfPermit()) {
            gameRooms.put(0, new GameRoom(0, "test game name", "test map name"));
        }
    }

    public void addWaitingPlayer(final Player player) {
        try (Permit permit = selfPermit()) {
            players.add(player);
        }
    }

    public GameRoom getGameRoom(final int gameId) {
        try (Permit permit = selfPermit()) {
            return gameRooms.get(gameId);
        }
    }

    public Permit getMovePlayerToGameRoomPermit(final Player player) {
        return issuePermit("movePlayerToGameRoom", players.contains(player));
    }

    public void movePlayerToGameRoom(final Player player, final GameRoom gameRoom) {
        checkForPermit("movePlayerToGameRoom");

        players.remove(player);
    }

    public Permit getMovePlayerToWaiting(final Player player) {
        return issuePermit("movePlayerToWaiting", !players.contains(player));
    }

    public void movePlayerToWaiting(final Player player, final GameRoom gameRoom) {
        checkForPermit("movePlayerToWaiting");

        players.add(player);
    }

    public List<GameRoom> getGameRooms() {
        try (Permit permit = selfPermit()) {
            return new ArrayList<>(gameRooms.values());
        }
    }

    public GameRoom create(String name, String map) {
        try (Permit permit = selfPermit()) {
            int newId;
            do {
                newId = ThreadLocalRandom.current().nextInt();
            } while (gameRooms.containsKey(newId));

            GameRoom gameRoom = new GameRoom(newId, name, map);
            gameRooms.put(gameRoom.getGameRoomId(), gameRoom);
            return gameRoom;
        }
    }

    public void removeGameRoom(GameRoom gameRoom) {
        try (Permit permit = selfPermit()) {
            gameRooms.remove(gameRoom.getGameRoomId());
        }

    }

}
