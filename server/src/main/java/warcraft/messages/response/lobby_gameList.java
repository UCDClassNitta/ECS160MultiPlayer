package warcraft.messages.response;

import warcraft.entity.GameRoomManager;
import warcraft.view.GameItem;

import java.util.Collection;
import java.util.stream.Collectors;

public class lobby_gameList extends AbstractResponseMessage {

    public final Collection<GameItem> games;

    public lobby_gameList(final GameRoomManager gameRoomManager) {
        games = gameRoomManager.getGameRooms().stream().map(GameItem::new).collect(Collectors.toList());
    }

}
