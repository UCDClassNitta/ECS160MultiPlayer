package warcraft.view;

public class PlayerItem {

    public final String name;
    public final String race;

    public PlayerItem(final warcraft.entity.Player player) {
        this.name = player.getName();
        this.race = player.getRace().toString();
    }
}