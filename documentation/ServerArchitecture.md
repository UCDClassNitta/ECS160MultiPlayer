# Server Architecture

### Components

#### Entities
The entities contain only the logic to manage their own data. The permit pattern is used to make concurrent changes to
multiple entities at the same time.

They are Network agnostic, and they can't call any function outside their classes. Thin entities


#### Controllers

There are multiple controllers: each message coming from the client has his own controller.
Each controller extends the class AbstractRequestMessage that allows to deserialize the client message,
and handle automatically the specific request in the server environment.

The controller are the only one allowed to communicate with entities and to send messages back to the client.

#### Views
The server doesn't expose the entities to the clients, therefore in the view directory, are contained the
Item in which we can encapsulate the entities, showing only the right fields.

#### Network Handlers

* *WarcraftServer*: starts listening on the port 8007, a set the handler and the channel initializer
* *WarcraftChannelInitializer*: encodes and decodes the messages from the socket stream
* *WarcraftHandler*: associates a socket with a client, and dispatches the messages when they arrives to
the correct handler
