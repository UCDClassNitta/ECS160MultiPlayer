package warcraft.view;

import warcraft.entity.GameRoom;

public class GameItem {

    public final int game_id;
    public final String game_name;
    public final String map;

    public GameItem(final int game_id, final String game_name, final String map) {
        this.game_id = game_id;
        this.game_name = game_name;
        this.map = map;
    }

    public GameItem(GameRoom gameRoom) {
        this(gameRoom.getGameRoomId(), gameRoom.getName(), gameRoom.getMap());
    }
}
