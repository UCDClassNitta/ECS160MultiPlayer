package warcraft.entity;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Permitter {

    private static final ConcurrentHashMap<Thread, HashMap<String, ExternalPermit>> PERMITS = new ConcurrentHashMap<>();

    private final ReentrantLock lock = new ReentrantLock();

    protected Permit selfPermit() {
        return new Permit();
    }

    protected Permit issuePermit(final String name, final boolean OK) {
        ExternalPermit permit = new ExternalPermit(name, OK);
        registerPermit(name, permit);
        return permit;
    }

    protected void checkForPermit(final String name) {
        HashMap<String, ExternalPermit> map = PERMITS.get(Thread.currentThread());

        if (map == null) {
            throw new RuntimeException("Invalid permit");
        }

        synchronized (map) {
            ExternalPermit permit = map.get(name);

            if (permit == null || !permit.isValid()) {
                throw new RuntimeException("Invalid permit");
            }
        }
    }

    private void registerPermit(String name, ExternalPermit permit) {
        HashMap<String, ExternalPermit> map = PERMITS.computeIfAbsent(Thread.currentThread(), thread -> new HashMap<>());

        synchronized (map) {
            if (map.containsKey(name)) {
                throw new IllegalStateException("Permit already issued with name: " + name);
            }

            map.put(name, permit);
        }
    }


    private void deregisterPermit(ExternalPermit permit) {
        PERMITS.get(Thread.currentThread()).remove(permit.name);
    }

    public class Permit implements AutoCloseable {

        private Permit() {
            lock.lock();
        }

        public boolean isValid() {
            return true;
        }

        @Override
        public void close() {
            lock.unlock();
        }
    }

    private class ExternalPermit extends Permit {

        public final String name;
        public final boolean valid;

        private ExternalPermit(final String name, final boolean valid) {
            this.name = name;
            this.valid = valid;
        }

        @Override
        public boolean isValid() {
            return this.valid;
        }

        @Override
        public void close() {
            deregisterPermit(this);
            super.close();
        }
    }

}
