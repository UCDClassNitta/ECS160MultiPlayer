package warcraft.web;

import retrofit.client.Response;
import retrofit.http.*;

import java.util.Collection;

public interface IStatisticService {
    @POST("/stats/winner")
    @FormUrlEncoded
    Response winner(@Field("players") Collection<String> players, @Field("winner") String winner);
}
