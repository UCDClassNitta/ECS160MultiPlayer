package warcraft.web;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;

public interface ILoginService {
    @GET("/login")
    @FormUrlEncoded
    boolean login(@Field("username") String user, @Field("password") String password);

    @GET("/logout")
    @FormUrlEncoded
    boolean logout(@Field("username") String user);
}
