## Buffered Lockstep
##### Detailing interaction between host clients, join clients, and server
----

## General info

We will be using the following nomenclature and values:

A tick is one game cycle as defined by the linux code.

A turn is a combination of **3** ticks and is the duration for which messages are 'bundled' (received together)

## For all clients

We will assume the architecture uses a single, thread-safe queue to manage the buffering. (Using dual buffers and
swapping can be described upon request)

Clients should bootstrap themselves by populating the queue with 6, (2 turns, 3 ticks per turn) no-actions for all
players.

--

`canTick` should work by determining if the queue has an action to perform; no item -> return `false`.

`sendAction` should be executed after every tick (implementations may buffer internally and send 3 discrete actions at
 the end of a turn).

`receiveAction` should be executed in a separate thread or asynchronously.

- If run in a separate thread (or using a callback function), you should append the actions you receive to the queue
immediately.

- If you use a future or promise to represent the execution, you would want to query the future at the end of the turn.
If there is data, appened it. If there is no data, the game should stall (while ensuring that the future is
continuously queried.

### If a hosting client

Synopsis: You will need to run the AI agent and send your actions as well as the actions for each AI player and
 "player0" (PlayerColor = 0).

As a host, you will run the AI agent and relay the actions it generates. You are also responsible for recording the
actions player0 generates.

If there is no action, you must generate a no-action event and send that instead.

Additionally, you must send player0 actions and AI player actions before you send your own action. Any player0 or AI
action sent after you sent your own action will be dropped.

### If a joining client

No work is necessary except for the boostrapping. The server will send all actions, including no-actions.





