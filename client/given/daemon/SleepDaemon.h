#ifndef SLEEPDAEMON_H
#define SLEEPDAEMON_H

#include "Daemon.h"

class CSleepDaemon : public CDaemon{
    protected:
        long DSleepTime;
        bool DWaiting;

        bool SignalHandler(int param);
        bool PreForkStart();
        bool PreForkCleanup();
        bool PostForkCleanup();
        bool Execute();
        bool Command(int argc, char *argv[]);
        bool FileEvent(int fd, short event);
        
    public:
        CSleepDaemon(long sleeptime);
        ~CSleepDaemon();
};

#endif

