package warcraft.messages.response;

public class lobby_joinGameResponse extends AbstractResponseMessage {

    public final boolean success;

    public lobby_joinGameResponse(final boolean success) {
        this.success = success;
    }

}
