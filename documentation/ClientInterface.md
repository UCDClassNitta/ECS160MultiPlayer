## Clients Interface with Networking

### Goal

Every single client-platform, will have to communicate through this interface, in this way it will be possible
exchanging messages between the different platforms.

### Flow Idea

To simplify the integration with the networking, every client should apply a clear distinction between input of user actions and their output. 

For example, if a client clicks on "build unit X", the flow should be:

    userClickHandler(): receives the click and encapsulate the command inside cmd
    commandDispatch(cmd): enqueue the command inside the list of the next commands
      
    commandExecuterHandler(): every x ms, check if there are cmds in the queue and executes them.
    
With the networking the approach would be slightly different:

    userClickHandler(): receives the click and encapsulate the command inside cmd
    +  commandDispatchToNetwork(cmd): sends the message to the server
      
    +  networkHandler(receivesCmd): receive the cmds from the server
    commandDispatch(cmd): enqueue the command inside the list of the next commands
      
    commandExecuterHandler(): every x ms, check if there are cmds in the queue and executes them.


## Requirements

### Units Ids
All the units and object must have a numeric id. In this way, every object will have a unique reference number
to make actions

The generation method is still in decision phase.
Probably, there will be an method that will return it, e.g.

    int getNextId(PLAYER_ID); 

### Command encapsulation

All the commands must be encapsulated inside a standard struct:

    cmd: {
        Action action;
        int actors[]; // units' id
        Color TargetColor;
        AssetType TargetType;
        Position TargetLocation;
    }

TODO: Also the target should be identified with an ID instead of the position

See below how the types of cmd are implemented:

##### Action 

    Action enum{
        actNone = 0,
        actBuildPeasant = 1,
        actBuildFootman = 2,
        actBuildArcher = 3,
        actBuildRanger = 4,
        actBuildFarm = 5,
        actBuildTownHall = 6,
        actBuildBarracks = 7,
        actBuildLumberMill = 8,
        actBuildBlacksmith = 9,
        actBuildKeep = 10,
        actBuildCastle = 11,
        actBuildScoutTower = 12,
        actBuildGuardTower = 13,
        actBuildCannonTower = 14,
        actMove = 15,
        actRepair = 16,
        actMine = 17,
        actBuildSimple = 18,
        actBuildAdvanced = 19,
        actConvey = 20,
        actCancel = 21,
        actBuildWall = 22,
        actAttack = 23,
        actStandGround = 24,
        actPatrol = 25,
        actWeaponUpgrade1 = 26,
        actWeaponUpgrade2 = 27,
        actWeaponUpgrade3 = 28,
        actArrowUpgrade1 = 29,
        actArrowUpgrade2 = 30,
        actArrowUpgrade3 = 31,
        actArmorUpgrade1 = 32,
        actArmorUpgrade2 = 33,
        actArmorUpgrade3 = 34,
        actLongbow = 35,
        actRangerScouting = 36,
        actMarksmanship = 37,
        actMax = 38
    } 

##### Color 

    Color: enum{
        pcNone = 0,
        pcBlue = 1,
        pcRed = 2,
        pcGreen = 3,
        pcPurple = 4,
        pcOrange = 5,
        pcYellow = 6,
        pcBlack = 7,
        pcWhite = 8,
        pcMax = 9
    }

##### AssetType

    AssetType: enum{
        atNone = 0,
        atPeasant = 1,
        atFootman = 2,
        atArcher = 3,
        atRanger = 4,
        atGoldMine = 5,
        atTownHall = 6,
        atKeep = 7,
        atCastle = 8,
        atFarm = 9,
        atBarracks = 10,
        atLumberMill = 11,
        atBlacksmith = 12,
        atScoutTower = 13,
        atGuardTower = 14,
        atCannonTower = 15,
        atMax = 16
    }

##### Position

    Position: {
      int X, Y
    }


## The interfaces

### The Game Interface

The interface could be applied to both localhost game, and networking game. 
In the localhost game at the beginning could seem redundant, but ensures the clear division between the cause-effect of the user's action, and simplify the further development of the networking integration. In fact
if a common interface is applied, to switch from local game to network game, will be enough switching the class that implements the interface, without other major changes in the current code. It would make the client code totally indipendent from the networking.

**The current interface is only a draft!**

    interface: {
      createGame()
      
      // blocking call (or async with callback) that release only when all the client are synchronized and ready to start
      startGame()
      sendAction(cmd)
      
      // in game function
      int getNextId(color)
      
      //game
      canTick() //checked at each tick, if false, stop the game, if true, restart it and get the next cmd
      getActions() // get a list of actions to be executed in the same tick
      
      // callbacks function
      gameTerminated()
    }

##### Pseudo implementation of local interface

    interface: {
      createGame()
      startGame() { return true; }
      sendAction(cmd) { enqueue(cmd);  }
      
      // in game function
      int getNextId(color) { static int id; return id++; }
      
      //game
      canTick(){ return true }
      getActions(){ return dequeue(cmd)  }
        
      // callbacks function
      gameTerminated()
    }

#### Multiplayer Flow

##### startGame()
It should be the action that synchronize all the client before actually playing.
For this reason must be called when the game has been loaded, and wait until the multiplayer stack receives the confirmation by all the clients.
Once all the client are synchronized, starts the game and proceed with the normal LockStep or other implementation.

Local: in the local, the interface doesn't need to block, returning true, starts immediately the game.

### The Lobby Interface

The Lobby Interface allows the client developer to retrieve information about the current gameRoom that are waiting for new players, and allows the user to join one of them.

    interface: {
      authenticate(user, pass)
      
      getGameList()
      joinGame(gameId)
    }
