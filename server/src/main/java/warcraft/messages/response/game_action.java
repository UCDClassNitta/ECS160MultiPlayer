package warcraft.messages.response;

import com.google.gson.JsonObject;

public class game_action extends AbstractResponseMessage {

    public final JsonObject data;

    public game_action(final JsonObject data) {
        this.data = data;
    }

}
