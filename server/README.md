# Server #

## Import project in Intellij

- Download the project files


        git clone https://github.com/UCDClassNitta/ECS160MultiPlayer.git


- Open IntelliJ and create project from sources

- Select the server directory

- Import project from external source: Gradle

- Check the option "Use auto import"

- Finish



## Run the project

### Trough Commmand line

Install gradle

```bash
sudo apt-get install gradle

cd ./server/
make
make run
```


### Trough IntelliJ

- Go to the menu Run
- Edit Configurations
- New (+)
- Gradle
- Set the configuration

Name: run

Gradle Project: select the server/build.gradle file

Tasks: run

Now you have the green start button available. Run! :)


## Check the execution of the ping through netcat / putty

    nc 127.0.0.1 8007

    OR

    putty -> raw 127.0.0.1 8007

server responds to json (and arbitrary text with errors)
