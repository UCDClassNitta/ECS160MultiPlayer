package warcraft.entity;


import io.netty.channel.Channel;

public class Player extends Permitter {

    public enum PlayerState {
        ANONYMOUS, LOGGED, INGAME, DISCONNECTED
    }

    public enum PlayerRace {
        HUMAN, ORC
    }

    private final Channel channel;
    private String name;
    private PlayerRace race;
    private PlayerState state;

    private GameRoom gameRoom;

    public Player(Channel c) {
        channel = c;
        state = PlayerState.ANONYMOUS;
    }

    public Permit getLoginPermit() {
        return issuePermit("login", state == PlayerState.ANONYMOUS);
    }

    public void login(String name) {
        checkForPermit("login");

        this.state = PlayerState.LOGGED;
        this.name = name;
    }

    public Permit getSubscribePermit() {
        return issuePermit("subscribe", state == PlayerState.LOGGED);
    }

    public void subscribe(GameRoom gameRoom) {
        checkForPermit("subscribe");

        this.gameRoom = gameRoom;
        this.race = PlayerRace.HUMAN;
        this.state = PlayerState.INGAME;
    }

    public Permit getUnsubscribePermit() {
        return issuePermit("unsubscribe", state == PlayerState.INGAME);
    }

    public void unsubscribe() {
        checkForPermit("unsubscribe");

        this.gameRoom = null;
        this.race = null;
        this.state = PlayerState.LOGGED;
    }

    public boolean isAuthticated() {
        try (Permit permit = selfPermit()) {
            return state != PlayerState.ANONYMOUS;
        }
    }

    // ####################### GETTER AND SETTER SECTION #########################à

    public GameRoom getGameRoom() {
        try (Permit permit = selfPermit()) {
            return gameRoom;
        }
    }

    public boolean hasGameRoom() {
        try (Permit permit = selfPermit()) {
            return gameRoom != null;
        }
    }

    public Channel getChannel() {
        return channel;
    }

    public String getName() {
        try (Permit permit = selfPermit()) {
            return name;
        }
    }

    public PlayerRace getRace() {
        try (Permit permit = selfPermit()) {
            return race;
        }
    }

}