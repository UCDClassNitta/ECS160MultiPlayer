package warcraft.web;

import retrofit.RestAdapter;
import warcraft.WarcraftServer;

public class ApiService {

    public static String TOKEN = "ASD)UASD)AUNsadjaisj09";

    public static RestAdapter getRestAdapter() {
        return new RestAdapter.Builder()
                .setEndpoint(WarcraftServer.API_URL)
                .build();
    }

    public static ILoginService factoryAuth() {
        return getRestAdapter().create(ILoginService.class);
    }

    public static IStatisticService factoryStat() {
        return getRestAdapter().create(IStatisticService.class);
    }

    public static String getToken() {
        return TOKEN;
    }

}
