package warcraft;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import warcraft.Serializer.SerializationException;
import warcraft.messages.response.AbstractResponseMessage;

import java.util.List;
import java.util.logging.Level;

/**
 * Initializes channel to 'speak' messages
 */
class WarcraftChannelInitializer extends ChannelInitializer<SocketChannel> {

    static final int MESSAGE_LENGTH = 8388608;

    public final ChannelHandler handler;

    public WarcraftChannelInitializer(final ChannelHandler handler) {
        this.handler = handler;
    }

    @Override
    public void initChannel(final SocketChannel ch) throws Exception {
        ch.pipeline().addLast(
                // logging
                WarcraftServer.LOGGING_HANDLER,

                // in
                new DelimiterBasedFrameDecoder(WarcraftChannelInitializer.MESSAGE_LENGTH, Delimiters.lineDelimiter()),
                new StringDecoder(CharsetUtil.US_ASCII),
                new MessageDecoder(),

                // out
                new StringEncoder(CharsetUtil.US_ASCII),
                new MessageEncoder()
        );
        // router
        ch.pipeline().addLast("warcraft", this.handler);
    }

    public static class MessageDecoder extends MessageToMessageDecoder<String> {


        @Override
        protected void decode(final ChannelHandlerContext ctx, final String msg, final List<Object> out) throws Exception {
            final Object obj = Serializer.decode(msg);
            out.add(obj);
        }

        @Override
        public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
            final Throwable nestedCause = cause.getCause();
            final String errorMessage;
            if (nestedCause instanceof SerializationException) {
                final String encodedError = ((SerializationException) nestedCause).getEncodedMessage();

                errorMessage = nestedCause.getMessage();

                final ByteBuf buffer = ctx.alloc().buffer();
                buffer.writeBytes(
                        encodedError.getBytes(CharsetUtil.US_ASCII)
                );
                ctx.writeAndFlush(buffer);
            } else {
                errorMessage = cause.getMessage();
            }


            WarcraftServer.LOGGER.log(Level.WARNING, "Error decoding message: " + errorMessage);
        }
    }

    public static class MessageEncoder extends MessageToMessageEncoder<AbstractResponseMessage> {

        @Override
        protected void encode(final ChannelHandlerContext ctx, final AbstractResponseMessage msg, final List<Object> out) throws Exception {
            final String bytes = Serializer.encode(msg);
            out.add(bytes);
        }

        @Override
        public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
            final Throwable nestedCause = cause.getCause();
            final String errorMessage;

            if (nestedCause instanceof SerializationException) {
                final String encodedError = ((SerializationException) nestedCause).getEncodedMessage();

                errorMessage = nestedCause.getMessage();

                final ByteBuf buffer = ctx.alloc().buffer();
                buffer.writeBytes(
                        encodedError.getBytes(CharsetUtil.US_ASCII)
                );
                ctx.writeAndFlush(buffer);
            } else {
                errorMessage = cause.getMessage();
            }

            WarcraftServer.LOGGER.log(Level.WARNING, "Error encoding message: " + errorMessage, cause);
        }
    }

}
