package warcraft.messages.response;

import warcraft.entity.GameRoom;

public class lobby_hostGameResponse extends AbstractResponseMessage {

    public final boolean success;
    public final int game_id;

    public lobby_hostGameResponse(final boolean success, final GameRoom gameRoom) {
        this.success = success;
        this.game_id = gameRoom.getGameRoomId();
    }

}
