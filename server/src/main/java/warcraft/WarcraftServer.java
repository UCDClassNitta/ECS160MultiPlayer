package warcraft;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import warcraft.entity.GameRoomManager;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Server that implements ChrisCraft: ToN server protocol
 */
public final class WarcraftServer {

    //TODO we need to port the settings in a FILE
    public static final int PORT = Integer.parseInt(System.getProperty("port", "8007"));
    public static final Level LOGLEVEL = Level.parse(System.getProperty("loglevel", "ALL"));
    public static final String API_URL = System.getProperty("apiurl", "localhost:8777");
    public static final boolean WEB_MODE = Boolean.parseBoolean(System.getProperty("webmode", "false"));
    public static final int TURN_LENGTH = Integer.parseInt(System.getProperty("turnlength", "3"));

    public static final Logger LOGGER = Logger.getLogger("warcraft");

    static {
        LOGGER.setLevel(LOGLEVEL);
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.ALL);
        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(handler);
    }

    public static final LoggingHandler LOGGING_HANDLER = new LoggingHandler("warcraft");
    public static final GameRoomManager GAME_ROOM_MANAGER = new GameRoomManager();

    public static void main(final String[] args) throws Exception {

        // Configure the server.
        final EventLoopGroup dispatch = new NioEventLoopGroup(1);
        final EventLoopGroup workers = new NioEventLoopGroup();
        try {
            final ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap
                    .group(dispatch, workers)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 100)
                    .handler(LOGGING_HANDLER)
                    .childHandler(new WarcraftChannelInitializer(new WarcraftHandler(GAME_ROOM_MANAGER)));

            // Start the server.
            // Wait until the server socket is closed.
            bootstrap.bind(PORT).sync().channel().closeFuture().sync();
        } finally {
            // Shut down all event loops to terminate all threads.
            dispatch.shutdownGracefully();
            workers.shutdownGracefully();
        }
    }

}